package br.com.joaocabraldev.rocketprovas.students.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity(name = "certification_answers")
public class CertificationAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "certification_id")
    private Certification certification;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    private UUID questionID;

    private UUID answerID;

    private boolean isCorrect;

    @CreationTimestamp
    private LocalDateTime createdAt;
    
}
