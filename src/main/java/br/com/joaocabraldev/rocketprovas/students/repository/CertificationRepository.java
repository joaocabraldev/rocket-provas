package br.com.joaocabraldev.rocketprovas.students.repository;

import br.com.joaocabraldev.rocketprovas.students.entity.Certification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CertificationRepository extends JpaRepository<Certification, UUID> {

    boolean existsByTechnologyAndStudentEmail(String technology, String email);

}
