package br.com.joaocabraldev.rocketprovas.students.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VerifyCertificationDTO {

    private String email;

    private String technology;

}
