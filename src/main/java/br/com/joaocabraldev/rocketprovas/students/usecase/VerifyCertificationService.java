package br.com.joaocabraldev.rocketprovas.students.usecase;

import br.com.joaocabraldev.rocketprovas.students.dto.VerifyCertificationDTO;
import br.com.joaocabraldev.rocketprovas.students.repository.CertificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VerifyCertificationService {

    private final CertificationRepository certificationRepository;

    public boolean execute(VerifyCertificationDTO dto) {
        return certificationRepository.existsByTechnologyAndStudentEmail(dto.getTechnology(), dto.getEmail());
//        return (dto.getEmail().equals("joaocabraldev@gmail.com") && dto.getTechnology().equals("Java"));

    }

}