package br.com.joaocabraldev.rocketprovas.students.controller;

import br.com.joaocabraldev.rocketprovas.students.dto.VerifyCertificationDTO;
import br.com.joaocabraldev.rocketprovas.students.usecase.VerifyCertificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final VerifyCertificationService verifyCertificationService;

    @PostMapping("/verifyIfHasCertification")
    public boolean verifyIfHasCertification(@RequestBody VerifyCertificationDTO dto) {
        return verifyCertificationService.execute(dto);
    }

}
