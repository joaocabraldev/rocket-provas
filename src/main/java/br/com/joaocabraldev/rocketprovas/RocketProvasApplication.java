package br.com.joaocabraldev.rocketprovas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RocketProvasApplication {

    public static void main(String[] args) {
        SpringApplication.run(RocketProvasApplication.class, args);
    }

}
