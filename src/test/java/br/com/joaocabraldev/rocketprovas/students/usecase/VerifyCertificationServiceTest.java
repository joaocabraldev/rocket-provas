package br.com.joaocabraldev.rocketprovas.students.usecase;

import br.com.joaocabraldev.rocketprovas.students.dto.VerifyCertificationDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class VerifyCertificationServiceTest {

    @Autowired
    private VerifyCertificationService underTest;

    @Test
    @DisplayName("Deve retornar verdadeiro com dados corretos")
    void executeIfRight() {
        VerifyCertificationDTO dto = VerifyCertificationDTO
                .builder()
                .email("joaocabraldev@gmail.com")
                .technology("Java")
                .build();

        boolean expected = true;
        boolean actual = underTest.execute(dto);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Deve retornar falso com dados errados")
    void executeIfWrong() {
        VerifyCertificationDTO dto = VerifyCertificationDTO
                .builder()
                .email("outremail@gmail.com")
                .technology("Outra Tecnologia")
                .build();

        boolean expected = false;
        boolean actual = underTest.execute(dto);

        Assertions.assertEquals(expected, actual);
    }
}