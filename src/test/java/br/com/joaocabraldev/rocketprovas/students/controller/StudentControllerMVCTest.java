package br.com.joaocabraldev.rocketprovas.students.controller;

import br.com.joaocabraldev.rocketprovas.students.dto.VerifyCertificationDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.net.MalformedURLException;
import java.net.URL;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class StudentControllerMVCTest {

    @LocalServerPort
    private int port;

    private final String baseURL = "http://localhost:%s/%s";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void verifyIfHasCertificationRight() throws MalformedURLException {
        VerifyCertificationDTO dto = VerifyCertificationDTO
                .builder()
                .email("joaocabraldev@gmail.com")
                .technology("Java")
                .build();

        String url = String.format(baseURL, port, "/students/verifyIfHasCertification");
        ResponseEntity<Boolean> response = restTemplate.postForEntity(new URL(url).toString(), dto, Boolean.class);

        boolean expected = true;
        boolean actual = response.getBody();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void verifyIfHasCertificationWrong() throws MalformedURLException {
        VerifyCertificationDTO dto = VerifyCertificationDTO
                .builder()
                .email("outremail@gmail.com")
                .technology("Outra Tecnologia")
                .build();

        String url = String.format(baseURL, port, "/students/verifyIfHasCertification");
        ResponseEntity<Boolean> response = restTemplate.postForEntity(new URL(url).toString(), dto, Boolean.class);

        boolean expected = false;
        boolean actual = response.getBody();

        Assertions.assertEquals(expected, actual);
    }
}